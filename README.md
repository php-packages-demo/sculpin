# [sculpin/sculpin](https://libraries.io/search?q=sculpin)

[**sculpin/sculpin**](https://packagist.org/packages/sculpin/sculpin) [651](https://phppackages.org/s/sculpin) Static Site Generator [https://sculpin.io](https://sculpin.io)

[![PHPPackages Rank](http://phppackages.org/p/sculpin/sculpin/badge/rank.svg)](http://phppackages.org/p/sculpin/sculpin)
[![PHPPackages Referenced By](http://phppackages.org/p/sculpin/sculpin/badge/referenced-by.svg)](http://phppackages.org/p/sculpin/sculpin)

## [Sculpin official documentation](https://sculpin.io/documentation/)
